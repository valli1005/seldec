package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class DuplicateLeadPage extends Annotations{ 

	public DuplicateLeadPage() {
       PageFactory.initElements(driver, this);
	} 

	@FindBy(how=How.XPATH, using="//a[text()='Duplicate Lead']") WebElement eleSubmitButton;
	public LoginPage createDuplicateLead() {
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		click(eleSubmitButton);  
		return new LoginPage();

	}
	
	




}



