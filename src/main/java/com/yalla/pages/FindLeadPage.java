package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class FindLeadPage extends Annotations{ 

	public FindLeadPage() {
       PageFactory.initElements(driver, this);
	} 
	@FindBy(how=How.XPATH, using="//button[text()='Find Leads']") WebElement eleFindLeads;
	@FindBy(how=How.XPATH, using="(//td[@class='x-grid3-col x-grid3-cell x-grid3-td-partyId x-grid3-cell-first ']/div/a)[1]") WebElement eleFirstSearchResult;
	
	public ViewLeadPage clickFirstSearchResult() {
		
		click(eleFirstSearchResult);
		return new ViewLeadPage(); 
	}
	
public FindLeadPage clickFindLeadButton() throws InterruptedException {
		
		click(eleFindLeads);
		Thread.sleep(2000);
		return this; 
	}

}







