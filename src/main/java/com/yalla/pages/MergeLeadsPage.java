package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class MergeLeadsPage extends Annotations{ 

	public MergeLeadsPage() {
       PageFactory.initElements(driver, this);
	} 
	@FindBy(how=How.XPATH, using="(//img[@src ='/images/fieldlookup.gif'])[1]") WebElement eleFromLead;
	
	public MergeLeadsPage clickFromName(String data) {
		
		click(eleFromLead);
		
		return this; 
	}
@FindBy(how=How.ID, using="viewLead_lastName_sp") WebElement eleViewLastName;
	
	public MergeLeadsPage verifyLastName(String data) {
		
		verifyExactText(eleViewLastName, data);
		
		return this; 
	}

	
@FindBy(how=How.XPATH, using="//a[text()='Edit']") WebElement eleEditButton;
	
	public EditLeadPage clickEditButton() {
		
	click(eleEditButton);
	
		return new EditLeadPage(); 
	}
	
@FindBy(how=How.CLASS_NAME, using="subMenuButtonDangerous") WebElement eleDeleteButton;
	
	public MyLeadsPages clickDeleteButton() {
		
	click(eleEditButton);
	
		return new MyLeadsPages(); 
	}
	
@FindBy(how=How.XPATH, using="//a[text()='Duplicate Lead']") WebElement eleDuplicateButton;
	
	public DuplicateLeadPage clickDuplicateButton() {
		
	click(eleDuplicateButton);
	
		return new DuplicateLeadPage(); 
	}
	
	
	
	@FindBy(how=How.LINK_TEXT, using="Logout") WebElement eleViewLeadLogout;
	public LoginPage clickViewLeadLogout() {
		//WebElement eleLogout = locateElement("class", "decorativeSubmit");
		click(eleViewLeadLogout);  
		return new LoginPage();

	}
	

}







