package com.yalla.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.pages.LoginPage;
import com.yalla.testng.api.base.Annotations;

public class TC004_DeleteLead extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC005_DuplicateLead";
		testcaseDec = "Duplicate a Lead";
		author = "suvetha";
		category = "smoke";
		excelFileName = "TC001";
	} 

	@Test(dataProvider="fetchData") 
	public void createLead(String uName, String uPassword) throws InterruptedException {
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(uPassword)
		.clickLogin()
		.clickCRMSFA()
		.clickLeads()
		.clickFindLead()
		.clickFindLeadButton()
		.clickFirstSearchResult()
		.clickDuplicateButton();
		
		
		
		
	}
		
		
		
		/*LoginPage lp = new  LoginPage();
		lp.enterUserName();
		lp.enterPassWord();*/
		
	}
	







