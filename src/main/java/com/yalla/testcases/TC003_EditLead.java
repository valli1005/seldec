package com.yalla.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.pages.LoginPage;
import com.yalla.testng.api.base.Annotations;

public class TC003_EditLead extends Annotations{
	
	@BeforeTest
	public void setData() {
		testcaseName = "TC003_EditLead";
		testcaseDec = "Editing a Lead";
		author = "suvetha";
		category = "smoke";
		excelFileName = "TC003";
	} 

	@Test(dataProvider="fetchData") 
	public void createLead(String uName, String uPassword,String firstName, String lastName) throws InterruptedException {
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(uPassword)
		.clickLogin()
		.clickCRMSFA()
		.clickLeads()
		.clickFindLead()
		.clickFindLeadButton()
		.clickFirstSearchResult()
		.clickEditButton()
		.editFirstname(firstName)
		.editLastname(lastName)
		.clickUpdateButton()
		.verifyFirstName(firstName)
		.verifyLastName(lastName);
	
		
		
		
		
	}
		
		
		
		/*LoginPage lp = new  LoginPage();
		lp.enterUserName();
		lp.enterPassWord();*/
		
	}
	







